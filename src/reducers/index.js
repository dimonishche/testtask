import {combineReducers} from 'redux'
import {global} from '../actions/global'
import {alerts} from '../actions/alerts'
import {notificationReceivers} from '../actions/notificationReceivers'
import {reducer as formReducer } from 'redux-form'

const rootReducer = combineReducers({
  global,
  alerts,
  notificationReceivers,
  form: formReducer
})

export default rootReducer
