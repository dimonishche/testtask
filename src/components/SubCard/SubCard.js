import React from 'react'
import './SubCard.css'

const SubCard = ({cardTitle, collapsed, children, toggleCard, bodyClass}) => {
  return (
    <div className='subCard'>
      <div className='subCardTitle'>
        <h2>{cardTitle}</h2>
        <label onClick={toggleCard} className='tab toggleTabs'>{collapsed ? 'EXPAND' : 'COLLAPSE'}</label>
      </div>
      <div className={'subCardBody ' + bodyClass}>
        {children}
      </div>
    </div>
  )
}

export default SubCard