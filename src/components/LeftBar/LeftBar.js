import React from 'react'
import FontAwesome from 'react-fontawesome'
import './LeftBar.css'

const LeftBar = ({leftBarItems, activeLeftBarItem, changeActiveBarItem}) => {
  return (
    <div className='leftBar'>
      <ul className='leftBarItemsList'>
        {leftBarItems.map((item, index) =>
          <li
            className={(item.active ? 'leftBarItem activeItem' : 'leftBarItem')}
            onClick={() => changeActiveBarItem(item.name)}
            key={index}>
            <FontAwesome className='leftBarIcon'
                         name={item.icon}/>
            <p>{item.name}</p>
          </li>
        )}
      </ul>
    </div>
  )
}

export default LeftBar
