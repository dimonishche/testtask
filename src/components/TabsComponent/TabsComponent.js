import React from 'react'
import './TabsComponent.css'

class TabsComponent extends React.Component {
  state = {
    activeTab: ''
  }
  
  componentDidMount() {
    this.setState({activeTab: this.props.tabs[0].name})
  }
  
  handleClick = (name) => {
    this.setState({activeTab: name})
  }
  
  render() {
    const {tabs, label, labelClass, tabsClass} = this.props
    return (
      <div className={'tabs ' + (!!tabsClass ? tabsClass : '')}>
        {!!label ? <label className={labelClass}>{label}</label> : null}
        {tabs.map((item, index) =>
          <label onClick={() => this.handleClick(item.name)} key={index} className={'tab ' + (this.state.activeTab === item.name ? 'activeTab' : '')}>{item.name}</label>
        )}
      </div>
    )
  }
}

export default TabsComponent