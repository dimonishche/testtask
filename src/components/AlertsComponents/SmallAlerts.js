import React from 'react'
import Card from '../Card'
import './SmallAlerts.css'
import Tabs from '../TabsComponent'

const SmallCard = ({cardTitle, rangeValue, maxValue, labelLeftPosition, handlerRangeValue, handlerChangeMaxValue, alertDescription, inputName}) => {
  return (
    <Card cardTitle={cardTitle} cardClass='smallCard'>
      <Tabs
        tabs={[{name: 'ENABLED'}, {name: 'DISABLED'}]}
      />
      <p className='alertDescription'>{alertDescription}</p>
      <div className='rangeBlock'>
        <label className='inputLabel' htmlFor={inputName}>Limitation per day</label>
        <span className='minValue'>1</span>
        <input className='rangeInput' id={inputName} name={inputName} type='range' min='1' max={maxValue} value={rangeValue} onChange={handlerRangeValue}/>
        <input className='maxLimit inputStyle' name={`maxValue${inputName}`} id={`maxValue${inputName}`} min='1' type="number" value={maxValue} onChange={handlerChangeMaxValue}/>
        <label style={{left: labelLeftPosition + 'px'}} className='rangeValueLabel'>{rangeValue}</label>
      </div>
    </Card>
  )
}

export default SmallCard