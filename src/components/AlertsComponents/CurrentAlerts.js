import React from 'react'
import Card from '../Card'
import Tabs from '../TabsComponent'

const CurrentAlerts = ({currentAlerts, checkAlert, checkAllAlerts, enableAlerts, disableAlerts, removeAlerts}) => {
  return (
    <Card cardTitle='Current alerts' cardClass='currentAlerts'>
      <div className='currentAlertsContent'>
        <div className='currentAlertsButtons'>
          <button onClick={enableAlerts} className='buttonStyle enableButton'>ENABLE</button>
          <button onClick={disableAlerts} className='buttonStyle disableButton'>DISABLE</button>
          <button className='buttonStyle editButton'>EDIT</button>
          <button className='buttonStyle createButton'>CREATE</button>
          <button onClick={removeAlerts} className='buttonStyle removeButton'>REMOVE</button>
        </div>
        <Tabs 
          label='Alerts count period'
          labelClass='inlineLabel'
          tabs={[{name: 'DAY'}, {name: 'WEEK'}, {name: 'MONTH'}]}
        />
        <table className='events'>
          <thead className='eventsHead'>
          <tr>
            <th>
              <input onChange={(e) => checkAllAlerts(e.target.checked)} type='checkbox' id='checkAll'/>
              <label htmlFor='checkAll' className='alertCheckboxLabel'> </label>
            </th>
            <th>EVENT</th>
            <th>STATE</th>
            <th>NOTIFY</th>
            <th className='tableAlignRight'>FIRED</th>
            <th className='tableAlignRight'>RECEIVERS</th>
          </tr>
          </thead>
          <tbody>
          {currentAlerts.map(item =>
            <tr key={item.id}
                className={'eventsRow ' + (item.checked ? 'checked ' : '') + (!item.enabled ? 'disabled' : '')}>
              <td>
                <input
                  onChange={() => checkAlert(item.id)}
                  type='checkbox' id={'alertCheckbox' + item.id}
                  name={'alertCheckbox' + item.id}
                  checked={item.checked ? true : false}
                />
                <label htmlFor={'alertCheckbox' + item.id} className='alertCheckboxLabel'> </label>
              </td>
              <td>
                <p>{item.event}</p>
                <span className='eventLimit'>{item.limit}</span>
              </td>
              <td>{item.enabled ? 'Enabled' : 'Disabled'}</td>
              <td>{item.notify}</td>
              <td className='tableAlignRight'>{item.fired}</td>
              <td className='tableAlignRight'>{item.receivers}</td>
            </tr>
          )}
          </tbody>
        </table>
      </div>
    </Card>
  )
}

export default CurrentAlerts
