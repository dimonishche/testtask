import React from 'react'
import './Header.css'

const Header  = () => {
  return (
    <header className='header'>
      <div className='left'></div>
      <div className='headTitle'>
        <div className='circle'></div>
        <h1 className='headTitleName'>MANAGEMENT CONSOLE</h1>
      </div>
      <div className='headUser'>
        <div>
          <p className='userName'>Antony Collins</p>
          <p className='userRole'>Administrator</p>
        </div>
        <img className='userPhoto' src={require('../../images/userIcon.png')} alt=""/>
        <a className='signOut' href="/">SIGN OUT</a>
      </div>
    </header>
  )
}

export default Header
