import React from 'react'
import './Card.css'

const Card = ({cardTitle, cardClass, children}) => {
  return (
    <div className={'card ' + (!!cardClass ? cardClass : '')}>
      <h2 className='cardTitle'>{cardTitle}</h2>
      {children}
    </div>
  )
}

export default Card
