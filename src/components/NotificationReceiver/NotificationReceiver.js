import React from 'react'
import {Field, reduxForm} from 'redux-form'
import Card from '../Card'
import './NotificationReceiver.css'

const NotificationReceiver = ({invalid, handleSubmit}) => {
  return (
    <Card cardTitle='Notification receiver' cardClass='notificationReceiverCard'>
      <form onSubmit={handleSubmit}>
        <label className='inputLabel' htmlFor='name'>Name</label>
        <Field className='inputStyle notificationInput' id='name' name='name' component={renderField} type='text'
               label='Enter receiver name'/>
        <label className='inputLabel' htmlFor='email'>Email address</label>
        <Field className='inputStyle notificationInput' id='email' name='email' component={renderField} type='text'
               label='Enter receiver Email address'/>
        <label className='inputLabel' htmlFor='phone'>Phone number</label>
        <Field className='inputStyle notificationInput' id='phone' name='phone' component={renderField} type='text'
               label='Enter receiver phone number'/>
        <button className={'buttonStyle ' + (invalid ? 'disabledButton' : '')} type='submit'>ADD RECEIVER</button>
      </form>
    </Card>
  )
}

const renderField = ({input, id, className, label, type, meta: {touched, error}}) => {
  return (
    <div className='field'>
      <input {...input} placeholder={label} type={type}
                        className={`${className} ${touched && error ? 'errorField' : ''}`} id={id}/>
      {touched &&
      error ?
        <span className='inputError'>
          {error}
        </span> : <span className='inputError'></span>}
    </div>
  )
}

export const validate = values => {
  const errors = {};
  if (!values.name)
    errors.name = 'The field cannot be empty'
  if (!values.email)
    errors.email = 'The field cannot be empty'
  else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address'
  }
  if (!values.phone)
    errors.phone = 'The field cannot be empty'
  else if (!/^\+?([0-9]{12})$/.test(values.phone))
    errors.phone = 'Invalid phone number'

  return errors
}

export default reduxForm({form: 'notificationReceiver', validate})(NotificationReceiver)