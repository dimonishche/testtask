import React from 'react'
import Header from '../Header'
import Alerts from '../Alerts'
import LeftBar from '../../containers/LeftBar'
import './App.css'

const App  = () => {
  return (
    <div className='app'>
      <Header />
      <div className='appContent'>
        <LeftBar />
        <div className='rightContent'>
          <h1 className='contentTitle'>Alerts & Notifications</h1>
          <Alerts />
        </div>
      </div>
    </div>
  )
}

export default App
