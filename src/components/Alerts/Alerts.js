import React from 'react'
import CurrentAlerts from '../../containers/CurrentAlerts'
import SmsAlerts from '../../containers/SmsAlerts'
import EmailAlerts from '../../containers/EmailAlerts'
import DetailsCard from '../../containers/DetailsCard'
//import NotificationReceiver from '../NotificationReceiver'
import NotificationReceiver from '../../containers/NotificationReceiver'
import './Alerts.css'

const Alerts = () => {
  return (
    <div className='alertsContent'>
      <div className='leftAlerts'>
        <CurrentAlerts />
        <SmsAlerts />
        <EmailAlerts />
      </div>
      <DetailsCard />
      <NotificationReceiver />
    </div>
  )
}

export default Alerts

