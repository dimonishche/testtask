import React from 'react'
import Card from '../Card'
import SubCard from '../../containers/SubCard'
import Tabs from '../TabsComponent'

import './DetailsCard.css'

const DetailsCard = ({receivers, checkReceiver, deleteReceivers}) => {
  return (
    <Card cardTitle='Details' cardClass='detailsCard'>
      <div>
        <label htmlFor='eventCategory' className='inputLabel'>Event category</label>
        <select className='selectStyle' name='eventCategory' id='eventCategory'>
          <option value='1'>Category 1</option>
          <option value='2'>Category 2</option>
        </select>
        <label htmlFor='eventLimit' className='inputLabel'>Event</label>
        <select className='selectStyle' name='eventLimit' id='eventLimit'>
          <option value='1'>Event 1</option>
          <option value='2'>Event 2</option>
        </select>
      </div>
      <Tabs
        tabsClass='notTopMargin'
        label='Event state'
        labelClass='inputLabel tabsLabel'
        tabs={[{name: 'ENABLE'}, {name: 'DISABLE'}]}
      />
      <Tabs
        tabsClass='tabs'
        label='Send notification via'
        labelClass='inputLabel tabsLabel'
        tabs={[{name: 'BOTH'}, {name: 'SMS'}, {name: 'EMAIL'}]}
      />
      <button className='buttonStyle saveButton'>SAVE</button>
      <SubCard cardTitle='Notification receivers (1)'>
        <div className='notificationReceivers'>
          {receivers.map(item =>
            <div key={item.id} className={'notificationReceiver ' + (item.checked ? 'checkedReceiver' : '')}>
              <div className='receiverInfo'>
                <input onChange={() => checkReceiver(item.id)} type='checkbox' id={'receiver' + item.id}  className='checkReceiver'/>
                <label htmlFor={'receiver' + item.id} className='checkboxLabel'> </label>
                <div className='notificationReceiverInfo'>
                  <p className='receiverName'>{item.name}</p>
                  <p className='receiverEmail'>{item.email}</p>
                  <p className='receiverPhone'>{item.phone}</p>
                </div>
              </div>
              <span className='deleteReceiver' onClick={() => deleteReceivers(item.id)}>DELETE</span>
            </div>
          )}
        </div>
      </SubCard>
      <SubCard cardTitle='Alert fire dates (3)'>
        <div className='tabs'>
          <label className='tab activeTab'>DAY</label>
          <label className='tab'>WEEK</label>
          <label className='tab'>MONTH</label>
        </div>
        <div className='fireDates'>
          <p className='fireDate'>2017-Jan-03 Tue 09:13:21</p>
          <p className='fireDate'>2017-Feb-12 Fri 09:13:21</p>
          <p className='fireDate'>2017-Feb-23 Fri 09:13:21</p>
        </div>
      </SubCard>
    </Card>
  )
}

export default DetailsCard