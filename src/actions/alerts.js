import createReducer from '../createReducer'
import {currentAlerts} from '../constants/alerts'

const RECEIVE_CURRENT_ALERTS='RECEIVE_CURRENT_ALERTS'
const CHECK_ALERT='CHECK_ALERT'
const CHECK_ALL_ALERTS='CHECK_ALL_ALERTS'
const ENABLE_ALERTS='ENABLE_ALERTS'
const DISABLE_ALERTS='DISABLE_ALERTS'
const REMOVE_ALERTS='REMOVE_ALERTS'

export const receiveCurrentAlerts = () => ({
  type: RECEIVE_CURRENT_ALERTS, 
  currentAlerts: currentAlerts.map(item => ({checked: false, ...item}))
})

export const checkAlert = (id) => ({
  type: CHECK_ALERT,
  id
})

export const checkAllAlerts = (checked) => ({type: CHECK_ALL_ALERTS, checked})

export const enableAlerts = () => ({type: ENABLE_ALERTS})

export const disableAlerts = () => ({type: DISABLE_ALERTS})

export const removeAlerts = () => ({type: REMOVE_ALERTS})

const initialState = {
  currentAlerts: []
}

export const alerts = createReducer(initialState, {
  [RECEIVE_CURRENT_ALERTS]: (state, {currentAlerts}) => ({currentAlerts}),
  [CHECK_ALERT]: ({currentAlerts}, {id}) => ({
    currentAlerts: currentAlerts.map(item => ({...item, checked: (item.id === id ? !item.checked : item.checked)}))
  }),
  [CHECK_ALL_ALERTS]: ({currentAlerts}, {checked}) => ({
    currentAlerts: currentAlerts.map(item => ({...item, checked: checked}))
  }),
  [ENABLE_ALERTS]: ({currentAlerts}, action) => ({
    currentAlerts: currentAlerts.map(item => ({...item, enabled: (item.checked ? true : item.enabled)}))
  }),
  [DISABLE_ALERTS]: ({currentAlerts}, action) => ({
    currentAlerts: currentAlerts.map(item => ({...item, enabled: (item.checked ? false : item.enabled)}))
  }),
  [REMOVE_ALERTS]: ({currentAlerts}, action) => ({
    currentAlerts: currentAlerts.filter(item => item.checked === false)
  })
})