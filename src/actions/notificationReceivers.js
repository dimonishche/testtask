import createReducer from '../createReducer'
import {notificationReceiversItems} from '../constants/alerts'
import {getLastId} from '../utils'

const RECEIVE_NOTIFICATION_RECEIVERS = 'RECEIVE_NOTIFICATION_RECEIVERS'
const ADD_NOTIFICATION_RECEIVERS = 'ADD_NOTIFICATION_RECEIVERS'
const DELETE_NOTIFICATION_RECEIVERS = 'DELETE_NOTIFICATION_RECEIVERS'
const CHECK_RECEIVER='CHECK_RECEIVER'

export const receiveNotificationReceivers = () => ({
  type: RECEIVE_NOTIFICATION_RECEIVERS,
  receivers: notificationReceiversItems
})

export const addNotificationReceivers = ({name, email, phone}) => (dispatch, getState) => {
  const receivers = getState().notificationReceivers.receivers
  const id = getLastId(receivers, 'id')
  dispatch({
    type: ADD_NOTIFICATION_RECEIVERS,
    receiver: {
      id,
      name,
      email,
      phone
    }
  })
}

export const deleteReceivers = (id) => ({type: DELETE_NOTIFICATION_RECEIVERS, id})

export const checkReceiver = (id) => ({type: CHECK_RECEIVER, id})

const initialState = {
  receivers: []
}

export const notificationReceivers = createReducer(initialState, {
  [RECEIVE_NOTIFICATION_RECEIVERS]: (state, {receivers}) => ({
    receivers: receivers.map(item => ({
      ...item,
      checked: false
    }))
  }),
  [ADD_NOTIFICATION_RECEIVERS]: ({receivers}, {receiver}) => ({
    receivers: [...receivers, {...receiver}]
  }),
  [DELETE_NOTIFICATION_RECEIVERS]: ({receivers}, {id}) => ({
    receivers: receivers.filter(item => item.id !== id)
  }),
  [CHECK_RECEIVER]: ({receivers}, {id}) => ({
    receivers: receivers.map(item => ({...item, checked: (item.id === id ? !item.checked : item.checked)}))
  })
})