import createReducer from '../createReducer'
import {leftBarItems} from '../constants/leftBar'

const RECEIVE_LEFT_BAR_ITEMS='RECEIVE_LEFT_BAR_ITEMS'
const CHANGE_ACTIVE_BAR_ITEM='CHANGE_ACTIVE_BAR_ITEM'

export const receiveLeftBarItems = () => ({
  type: RECEIVE_LEFT_BAR_ITEMS,
  leftBarItems: leftBarItems.map(item => ({active: false, ...item}))
})

export const changeActiveBarItem = (activeLeftBarItem) => ({type: CHANGE_ACTIVE_BAR_ITEM, activeLeftBarItem})

const initialState = {
  leftBarItems: []
}

export const global = createReducer(initialState, {
  [RECEIVE_LEFT_BAR_ITEMS]: (state, {leftBarItems}) => ({leftBarItems}),
  [CHANGE_ACTIVE_BAR_ITEM]: ({leftBarItems}, {activeLeftBarItem}) => ({
    leftBarItems: leftBarItems.map(item => ({...item, active: (item.name === activeLeftBarItem)}))
  })
})