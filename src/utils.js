export const getLastId = (array, property) => {
  if (!array.length) {
    return 1
  }

  return Math.max.apply(null, array.map(item => item[property])) + 1
}