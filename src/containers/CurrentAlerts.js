import React from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import CurrentAlerts from '../components/AlertsComponents/CurrentAlerts'
import {
  receiveCurrentAlerts,
  checkAlert,
  checkAllAlerts,
  enableAlerts,
  disableAlerts,
  removeAlerts
} from '../actions/alerts'
import {receiveNotificationReceivers} from '../actions/notificationReceivers'

class CurrentAlertsContainer extends React.Component {
  componentWillMount() {
    this.props.receiveCurrentAlerts()
  }

  render() {
    return (
      <CurrentAlerts
        {...this.props}
      />
    )
  }
}

const mapStateToProps = ({alerts}) => {
  return {
    currentAlerts: alerts.currentAlerts
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  receiveCurrentAlerts,
  checkAlert,
  checkAllAlerts,
  enableAlerts,
  disableAlerts,
  removeAlerts,
  receiveNotificationReceivers
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(CurrentAlertsContainer)

