import React from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import LeftBar from '../components/LeftBar'
import {receiveLeftBarItems, changeActiveBarItem} from '../actions/global'

class LeftBarContainer extends React.Component {
  componentWillMount() {
    this.props.receiveLeftBarItems()
    this.props.changeActiveBarItem('alerts')
  }
  
  render() {
    return (
      <LeftBar {...this.props}/>
    )
  }
}

const mapStateToProps = ({global}) => {
  return {
    leftBarItems: global.leftBarItems
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({receiveLeftBarItems, changeActiveBarItem}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(LeftBarContainer)

