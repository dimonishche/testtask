import React from 'react'
import SmallCard from '../components/AlertsComponents/SmallAlerts'

class SmsAlerts extends React.Component {
  state = {
    smsValue: 1,
    maxValue: 300,
    labelLeftPosition: 22
  }

  handlerChangeSmsValue = (e) => {
    const value = e.target.value
    this.setState({smsValue: +value})
    this.changeLeftPosition(this.state.maxValue, value)
  }

  handlerChangeMaxValue = (e) => {
    const maxValue = +e.target.value
    this.setState({maxValue: maxValue})
    if (this.state.smsValue > maxValue && maxValue !== 0)
      this.setState({smsValue: maxValue})
    this.changeLeftPosition(maxValue, this.state.smsValue)
  }

  changeLeftPosition = (maxValue, smsValue) => {
    const smsValueLength = !!smsValue.length ? smsValue.length : 1
    const firstLabelPosition = 26 - smsValueLength * 6
    this.setState({labelLeftPosition: firstLabelPosition + 181 / maxValue * smsValue})
  }

  render() {
    return (
      <SmallCard
        cardTitle='SMS alerts'
        handlerRangeValue={this.handlerChangeSmsValue}
        handlerChangeMaxValue={this.handlerChangeMaxValue}
        alertDescription='SMS can be purchased individualy or by backages. As soon as package SMS used out new package is purchased automatically.'
        rangeValue={this.state.smsValue}
        maxValue={this.state.maxValue}
        labelLeftPosition={this.state.labelLeftPosition}
        inputName='smsAlerts'
      />
    )
  }
}

export default SmsAlerts
