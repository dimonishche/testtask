import React from 'react'
import SmallCard from '../components/AlertsComponents/SmallAlerts'

class EmailAlerts extends React.Component {
  state = {
    emailValue: 1,
    maxValue: 300,
    labelLeftPosition: 22
  }

  handlerChangeEmailValue = (e) => {
    const value = e.target.value
    this.setState({emailValue: +value})
    this.changeLeftPosition(this.state.maxValue, value)
  }

  handlerChangeMaxValue = (e) => {
    const maxValue = +e.target.value
    this.setState({maxValue: maxValue})
    if (this.state.emailValue > maxValue && maxValue !== 0)
      this.setState({emailValue: maxValue})
    this.changeLeftPosition(maxValue, this.state.emailValue)
  }

  changeLeftPosition = (maxValue, emailValue) => {
    const emailValueLength = !!emailValue.length ? emailValue.length : 1
    const firstLabelPosition = 26 - emailValueLength * 6
    this.setState({labelLeftPosition: firstLabelPosition + 181 / maxValue * emailValue})
  }

  render() {
    return (
      <SmallCard
        cardTitle='Email alerts'
        handlerRangeValue={this.handlerChangeEmailValue}
        handlerChangeMaxValue={this.handlerChangeMaxValue}
        alertDescription='Email gate has limited number of Emails which it can send trough the day from current domain. Please limit the number of sending Emails.'
        rangeValue={this.state.emailValue}
        maxValue={this.state.maxValue}
        labelLeftPosition={this.state.labelLeftPosition}
        inputName='emailAlerts'
      />
    )
  }
}

export default EmailAlerts
