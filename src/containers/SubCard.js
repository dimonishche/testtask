import React from 'react'
import SubCard from '../components/SubCard'

class SubCardContainer extends React.Component {
  state = {
    collapsed: false,
    bodyClass: ''
  }
  
  toggleCard = () => {
    this.setState({collapsed: !this.state.collapsed})
    if (!this.state.collapsed) {
      this.setState({bodyClass: 'subCardBodyHide'})
      setTimeout(() => this.setState({bodyClass: 'subCardBodyHidden'}), 500)
      return false
    }

    this.setState({bodyClass: 'subCardBodyShow'})
    setTimeout(() => this.setState({bodyClass: ''}), 500)
  }
  
  render() {
    return (
      <SubCard
        toggleCard={this.toggleCard}
        {...this.state}
        {...this.props}
      />
    )
  }
}

export default SubCardContainer