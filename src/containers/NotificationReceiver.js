import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import NotificationReceiver from '../components/NotificationReceiver'
import {addNotificationReceivers} from '../actions/notificationReceivers'

const mapDispatchToProps = dispatch => bindActionCreators({
  onSubmit: addNotificationReceivers
}, dispatch)

export default connect(null, mapDispatchToProps)(NotificationReceiver)

