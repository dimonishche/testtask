import React from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import DetailsCard from '../components/DetailsCard'
import {receiveNotificationReceivers, deleteReceivers, checkReceiver} from '../actions/notificationReceivers'

class DetailsCardContainer extends React.Component {
  componentWillMount() {
    this.props.receiveNotificationReceivers()
  }

  render() {
    return (
      <DetailsCard
        {...this.props}
      />
    )
  }
}

const mapStateToProps = ({notificationReceivers}) => {
  return {
    receivers: notificationReceivers.receivers
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  receiveNotificationReceivers,
  checkReceiver,
  deleteReceivers
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(DetailsCardContainer)

