export const leftBarItems = [
  {
    name: 'service',
    icon: 'server'
  },
  {
    name: 'users',
    icon: 'user-o'
  },
  {
    name: 'limits',
    icon: 'sliders'
  },
  {
    name: 'alerts',
    icon: 'warning'
  },
  {
    name: 'keys',
    icon: 'key'
  },
  {
    name: 'reports',
    icon: 'file-text-o'
  },
  {
    name: 'security',
    icon: 'shield'
  }
]