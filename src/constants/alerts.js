export const currentAlerts = [
  {
    id: 0,
    event: 'Signaling channels',
    limit: 'Limit reached',
    enabled: true,
    notify: 'SMS',
    fired: 3,
    receivers: 1
  },
  {
    id: 1,
    event: 'Storage',
    limit: 'Used over 50%',
    enabled: false,
    notify: 'Email',
    fired: 0,
    receivers: 0
  },
  {
    id: 2,
    event: 'Storage',
    limit: 'Used over 90%',
    enabled: true,
    notify: 'Both',
    fired: 0,
    receivers: 3
  },
  {
    id: 3,
    event: 'Data channels',
    limit: 'No free channels',
    enabled: true,
    notify: 'SMS',
    fired: 1,
    receivers: 2
  }
]

export const notificationReceiversItems = [
  {
    id: 0,
    name: 'George Cooling',
    email: 'g.coo@arhitech.com',
    phone: '+380745127612'
  },
  {
    id: 1,
    name: 'Alan Timber',
    email: 'alan-timber@arhitech.com',
    phone: '+380745127612'
  }
]